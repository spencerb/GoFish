//
// Created by spencer on 2/26/17.
//

#ifndef GOFISH_CARDRANKSTYPES_H
#define GOFISH_CARDRANKSTYPES_H

namespace Cards {
    enum Ranks {
        Unknown_Rank,
        Ace,
        Two,
        Three,
        Four,
        Five,
        Six,
        Seven,
        Eight,
        Nine,
        Jack,
        Queen,
        King
    };

    enum Suites {
        Unknown_Suite,
        Clubs,
        Hearts,
        Diamonds,
        Spades
    };
}

#endif //GOFISH_CARDRANKSTYPES_H
