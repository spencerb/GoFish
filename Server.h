//
// Created by spencer on 2/26/17.
//

#ifndef GOFISH_SERVER_H
#define GOFISH_SERVER_H
#include <vector>
#include <map>
#include <functional>

#include <SFML/Network.hpp>

#include "Player.h"


class Server {
public:
    Server();
    void update();
private:
    void createStartDeck();
    void shuffle();
    void serveRequest(std::string basic_string);
    void makeCommandMap();
    void dealHands();
    void transferHands();

    sf::TcpListener m_listener;
    sf::SocketSelector m_selector;
    sf::Packet m_packet;
    std::vector<sf::TcpSocket*> m_connectedSockets;

    std::map<std::string, std::function<void ()>> m_commandMap;
    std::vector<Card> m_52CardStandardDeck;
    std::vector<Player> m_players;
    int m_numReadyPlayers;
    bool gameHasStarted;
};


#endif //GOFISH_SERVER_H
