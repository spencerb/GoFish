//
// Created by spencer on 2/25/17.
//

#ifndef GOFISH_GAME_H
#define GOFISH_GAME_H

#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/Network.hpp>

#include <vector>

#include "Player.h"

class Game : public sf::NonCopyable{
public:
    Game();

    int run();
private:
    enum ConnectionStatus {
        PreDealRequest,
        AwaitingDealResponse,
        ActiveGame
    };

    void processEvents();
    void update(sf::Time deltatime);
    void render();

    /*  Network Communication helper methods    */
    bool connectToGameserver();
    void setupNetworkGame();
    void setupPlayer();

    sf::RenderWindow m_window;
    sf::TcpSocket m_socket;
    sf::Packet m_netBuffer;

    std::vector<Player> m_players;
    int m_connectionStatus;
    int m_numberOfPlayers;

    void fillDummyPlayers();

    void drawPlayers();
};


#endif //GOFISH_GAME_H
