//
// Created by spencer on 2/26/17.
//

#ifndef GOFISH_PLAYER_H
#define GOFISH_PLAYER_H

#include <vector>

#include <SFML/Graphics.hpp>
#include <SFML/Graphics/RenderStates.hpp>

#include "Card.h"

class Player : public sf::Drawable, public sf::Transformable {
public:
    Player();
    Player(std::vector<Card> startingHand);

    void setHand(std::vector<Card> handOfCards);
    std::vector<Card> getCards() const;

    bool isReal = false;

protected:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

private:
    std::vector<Card> m_cards;
};

sf::Packet& operator <<(sf::Packet& packet, const Player& player);

//sf::Packet& operator >>(sf::Packet& packet, Player& player);

#endif //GOFISH_PLAYER_H
