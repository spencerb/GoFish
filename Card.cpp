//
// Created by spencer on 2/26/17.
//

#include "Card.h"

#include <iostream>
Card::Card() {
    //do nothing
}

Card::Card(Cards::Suites suite, Cards::Ranks rank) {
    m_suite = suite;
    m_rank = rank;
}

void Card::setType(Cards::Suites suite, Cards::Ranks rank) {
    setSuite(suite);
    setRank(rank);
    //m_sprite.setTexture()
}

void Card::setRank(Cards::Ranks rank) {
    m_rank = rank;
}

void Card::setSuite(Cards::Suites suite) {
    m_suite = suite;
}

Cards::Suites Card::getSuite()const {
    return m_suite;
}

Cards::Ranks Card::getRank()const {
    return m_rank;
}

void Card::draw(sf::RenderTarget &target, sf::RenderStates states) const {
    states.transform *= getTransform();
    target.draw(m_sprite, states);
    std::cout << "Drawing works.  Get some textures on already!" << std::endl;
}


sf::Packet &operator>>(sf::Packet &packet, Card &card) {
    int rank;
    int suite;
    packet >> suite;
    packet >> rank;
    card.setType((Cards::Suites) suite, (Cards::Ranks) rank);
    return packet;
}

sf::Packet &operator<<(sf::Packet &packet, const Card &card) {
    return packet << card.getSuite() << card.getRank();
}
