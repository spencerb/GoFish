//
// Created by spencer on 2/26/17.
//
#include "Player.h"

Player::Player(std::vector<Card> startingHand) {
    m_cards = startingHand;
}

void Player::setHand(std::vector<Card> handOfCards) {
    m_cards = handOfCards;
}

std::vector<Card> Player::getCards() const {
    return m_cards;
}

void Player::draw(sf::RenderTarget& target, sf::RenderStates states) const {
    //draw each card.
    //TODO this needs to work for arbitrary numbers of cards between 0 and 52
    /* float spacePerCard = 1600 / m_cards.size();*/
    float spaceTaken = 0;
    for (auto card : m_cards) {
        card.setOrigin(spaceTaken,0);
        spaceTaken += 500; //TODO replace with card sprite size
        states.transform *= getTransform();
        target.draw(card, states);
    }
}

sf::Packet &operator<<(sf::Packet &packet, const Player &player) {
    for (Card card : player.getCards()) {
        packet << card;
    }
    return packet;
}

//the << operator was implemented for server sending, to prevent having to continually check the number of cards per player.
//the >> operator doesn't have to be implemented yet, but I think it will actually cut down on extraneous code in the Game class.
/*sf::Packet &operator>>(sf::Packet &packet, Player &player) {
    return packet;
}*/
