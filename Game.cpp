//
// Created by spencer on 2/25/17.
//

static const int minimum_frames_per_second = 35;

#include <iostream>
#include <cassert>

#include "Game.h"

Game::Game() : m_window(sf::VideoMode(800, 600), "Go Fish"), m_connectionStatus(ConnectionStatus::PreDealRequest) {
}

int Game::run() {
    sf::Clock clock;
    sf::Time timeSinceLastUpdate;
    sf::Time TimePerFrame = sf::seconds(1.f/minimum_frames_per_second);

    while (m_window.isOpen())
    {
        processEvents();

        timeSinceLastUpdate = clock.restart();
        while (timeSinceLastUpdate > TimePerFrame)
        {
            timeSinceLastUpdate -= TimePerFrame;
            update(TimePerFrame);
        }

        update(timeSinceLastUpdate);
        render();
    }
    return 0;
}

void Game::processEvents() {
    sf::Event event;
    while (m_window.pollEvent(event)) {
        if (event.type == sf::Event::Closed) {
            m_window.close();
        }
    }
}

void Game::render() {
    m_window.clear(sf::Color(50, 92, 46));//set background to green, for now, until we have a texture background.

    drawPlayers();

    m_window.display();
}

bool Game::connectToGameserver() {
    if (m_socket.connect(sf::IpAddress::LocalHost, 6193, sf::seconds(60)) != sf::Socket::Done) {
        std::cout << "Error: could not connect: Timeout." << std::endl;
        return false;
    }
    return true;
}

void Game::update(sf::Time deltatime) {
    switch (m_connectionStatus) {
        case ConnectionStatus::PreDealRequest:
            if (connectToGameserver()) {
                setupNetworkGame();
            }
            break;
        case ConnectionStatus::AwaitingDealResponse:
            setupPlayer();
            break;
        case ConnectionStatus::ActiveGame:
            break;//TODO: implemented
    }
}

void Game::setupNetworkGame() {
    std::string command;
    m_netBuffer.clear();
    command.clear();

    //this command should be delayed in sending by the client, until all players have started the game (Hit GO!?)
    //otherwise, the server will still wait to respond, possibly for a long time.
    m_netBuffer << command.append("deal");
    m_socket.send(m_netBuffer);
    m_connectionStatus = ConnectionStatus::AwaitingDealResponse;
}

void Game::setupPlayer() {
    std::string command;
    m_netBuffer.clear();
    command.clear();
    m_socket.receive(m_netBuffer);
    m_netBuffer >> command;
    std::cout << command << std::endl;
    if (command != "Hand") {//wrong response from server.  communications exception
    }

    m_netBuffer >> m_numberOfPlayers;

    //Can this be refactored somehow?  It looks horrible.
    std::vector<Card> dealtCards;
    if (m_numberOfPlayers == 2) {
        Card first, second, third, fourth, fifth;
        m_netBuffer >> first >> second >> third >> fourth >> fifth;
        dealtCards.push_back(first);
        dealtCards.push_back(second);
        dealtCards.push_back(third);
        dealtCards.push_back(fourth);
        dealtCards.push_back(fifth);
    } else if (m_numberOfPlayers >= 3 && m_numberOfPlayers <= 6) {
        Card first, second, third, fourth, fifth, sixth, seventh;
        m_netBuffer >> first >> second >> third >> fourth >> fifth >> sixth >> seventh;
        dealtCards.push_back(first);
        dealtCards.push_back(second);
        dealtCards.push_back(third);
        dealtCards.push_back(fourth);
        dealtCards.push_back(fifth);
        dealtCards.push_back(sixth);
        dealtCards.push_back(seventh);
    }

    Player player(dealtCards);

    //this Player::isReal member is just a way for clients to keep track of which player represents the user.
    player.isReal = true;
    m_players.push_back(player);
    fillDummyPlayers();
    m_connectionStatus = ConnectionStatus::ActiveGame;
}

void Game::fillDummyPlayers() {
    for (int i = 0; i < m_numberOfPlayers; ++i) {
        std::vector<Card> dummyCards;
        for (int o = 0; o < (m_numberOfPlayers >= 3 ? 5 : 7); ++o) {
            Card dummyCard(Cards::Suites::Unknown_Suite, Cards::Ranks::Unknown_Rank);
            dummyCards.push_back(dummyCard);
        }
        Player dummyPlayer(dummyCards);
        m_players.push_back(dummyPlayer);
    }
    assert(m_players.size() == m_numberOfPlayers);
}

void Game::drawPlayers() {
    //may have to handle transforms here.
    /*for (Player player : m_players) {
        m_window.draw(player);
    }*/
    //for testing, only handle 2 players at first.

    if (m_connectionStatus == AwaitingDealResponse) {
        Player& player = m_players.at(0);
        player.setOrigin(100, 1000);
        Player& player2 = m_players.at(1);
        player2.setOrigin(100, 300);
        player2.setRotation(180);
        m_window.draw(player);
        m_window.draw(player2);
    }
}
