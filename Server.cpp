//
// Created by spencer on 2/26/17.
//
#include <algorithm>
#include <iostream>

#include "Server.h"

Server::Server() : gameHasStarted(false) {
    m_listener.setBlocking(false);
    createStartDeck();
    m_selector.add(m_listener);
    if (m_listener.listen(6193) != sf::Socket::Done)
    {
        // error...
    }

    makeCommandMap();
}

void Server::update() {
    if (m_selector.wait(sf::seconds(5))) {
        if (m_selector.isReady(m_listener)) {
            sf::TcpSocket* s = new sf::TcpSocket();
            if (m_listener.accept(*s) != sf::Socket::Done) {
                //error
            }

            if (!gameHasStarted) {//allow connecting only before game has started.
                m_connectedSockets.push_back(s);
            } else {
                //TODO later, possibly add support in the server for multiple games.  Much later - big job.  child process?
                sf::Packet errorMsgBuffer;
                std::string errorResponse("Error");
                std::string errorMsg("A game is already in progress.  You cannot join.");
                errorMsgBuffer << errorResponse << errorMsg;
                s->send(errorMsgBuffer);
                s->disconnect();
            }
        }

        for (sf::TcpSocket* sock : m_connectedSockets) {
            if (m_selector.isReady(*sock)) {
                //receive data
                m_packet.clear();
                sock->receive(m_packet);
                std::string clientRequest;
                m_packet >> clientRequest;
                serveRequest(clientRequest);
            }
        }
    }
}

void Server::createStartDeck() {
    for (int rank = 1; rank <= 13; rank++) {
        for (int suite = 1; suite < 5; suite++) {
            Card card((Cards::Suites) suite, (Cards::Ranks) rank);
            m_52CardStandardDeck.push_back(card);
        }
    }
}

void Server::serveRequest(std::string basic_string) {
    //have map of string keys to function values, that I can populate at construction time.  Then check through map here.
    for (auto const &pair : m_commandMap) {
        if (pair.first == basic_string) {
            pair.second();
        }
    }
}

void Server::makeCommandMap() {
    m_commandMap.emplace("Deal", [&] () -> void {
        ++m_numReadyPlayers;
        if (m_numReadyPlayers >= m_connectedSockets.size() && m_numReadyPlayers >=2) {//all players are ready - start game: shuffle cards and deal hands.
            shuffle();
            dealHands();
        }
    });
}

void Server::dealHands() {

    //this might be better handled with an assert than an exception, but whatever.  I'll fix it later.
    for (int i = 0; i < m_connectedSockets.size(); i++) {
        std::vector<Card> playerHand;
        for (int o = 0; o < (m_connectedSockets.size() >= 3 ? 5 : 7); ++o) {
            Card card = m_52CardStandardDeck.back();
            m_52CardStandardDeck.pop_back();
            playerHand.push_back(card);
        }
        Player player(playerHand);
        m_players.push_back(player);
    }

    transferHands();
}

/*!
 * Randomly order Cards in vector
 */
void Server::shuffle() {
    //algorithmically shuffle deck.  fisher-yates/ knuth's.
    std::random_shuffle(m_52CardStandardDeck.begin(), m_52CardStandardDeck.end());

}

//TODO do we need something like a map between players and sockets?
void Server::transferHands() {
    for (int i = 0; i < m_numReadyPlayers; i++) {
        m_packet.clear();
        m_packet << "Hand" << m_numReadyPlayers << m_players.at(i);
        m_connectedSockets.at(i)->send(m_packet);
    }

}

int main() {
    Server server;
    bool serverShouldRun = true;
    while (serverShouldRun) {
        server.update();
    }
}

