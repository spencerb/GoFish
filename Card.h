//
// Created by spencer on 2/26/17.
//

#ifndef GOFISH_CARD_H
#define GOFISH_CARD_H

#include "CardRanksTypes.h"

#include <SFML/Graphics.hpp>
#include <SFML/Network/Packet.hpp>

class Card : public sf::Drawable, public sf::Transformable {
public:
    Card();
    Card(Cards::Suites, Cards::Ranks);

    void setType(Cards::Suites, Cards::Ranks);

    Cards::Suites getSuite()const;
    Cards::Ranks getRank()const;

protected:
    void draw(sf::RenderTarget &target, sf::RenderStates states) const override;

private:
    void setRank(Cards::Ranks);
    void setSuite(Cards::Suites);

    sf::Sprite m_sprite;
    Cards::Ranks m_rank = Cards::Ranks::Unknown_Rank;
    Cards::Suites m_suite = Cards::Suites::Unknown_Suite;
};

sf::Packet& operator <<(sf::Packet& packet, const Card& card);

sf::Packet& operator >>(sf::Packet& packet, Card& card);

#endif //GOFISH_CARD_H
